
# Chapter 4

## Integration Points
+ butterfly and spiderweb
+ 2N to 2^N connections
+ connections are the major soruce of problems

### socket based protocols

### non socket based protocols
+ named pipes
+ shared memory IPC

### connection failures
+ connection timeouts are measured in minutes
+ set socket timeouts
+ dropped ack can take minutes

### protocols
+ client syn
+ server synack or reset
+ client ack

### the 5am problem
+ tcpdump
+ wireshark
+ use tcpdump to get file and wireshard to analyze it offline

### packets
+ TCP three phase handshake
+ TCP/IP encapsulation
  + ethernet packet
    + IP packet
      + TCP packet
        + payload is HTTP request
+ The TCP/IP Guide [Koz05]
+ TCP/IP Illustrated [Ste93]

### Firewalls
+ TCP connections can exist indefinately
+ However firewall will close connections and not tell TCP
+ way too confusing war story
+ lesson: sometimes have to go below abstraction layers

### Server failure modes
+ accepts tcp connection but does not respond to HTTP
+ accepts HTTP request but does not "read requests"
+ sends a response that is unknown to the client
+ doesnt send json
+ always aquire and release locks in the same order
+ problems with synchronized methods (lots of java)

### Countering integration point problems
+ circuit breaker
+ test harnes

### Vendor api
+ random developers that write client code for big software vendors
+ full of bad coding and blocking operations

## Chain reactions
+ horizontal and vertical scaling
+ one node fails and puts stress on the others
+ other nodes are now more suseptable to failure
+ surviving nodes more likely to fail
+ bulkhead patterns create multiple pools

## Cascading Failures
+ upside down trees
+ services with high fan in are more suseptable
+ lower layers dont provide a way to distinguish serious failurs from transient ones

### Prevention
+ circuit breaker
+ timeouts

## Users

### Traffic
+ capacity
+ use autoscaling with caution

### Heap Memory
+ minimize in memory session
+ dead time between when the user stops and the application memory is recovered
+ weak references can be disposed of by the garbage collector

### off heap/host memory
+ local vs ousourced memory tradoff

### sockets
+ virtual IP addresses

### closed sockets
+ bogons

### expensive to serve
+ expensive users tradeoff

### unwanted users
+ competitive intelligence
+ circuit breaker
+ sessions and cookies
+ robots.txt tell can serve legitimate robots static content. keep prices secret
+ blocking robots Akamai, ARIN
+ engage legal

## Blocked threads
+ multithreading
+ "Java Concurrency in Practice" [Goe06] Brian Goetz
+ use external monitoring
+ blocked by shared resorces external resorces, caches etc
+ multithreaded programming seems to be an art

### problem definition
+ too many error conditions to test
+ unexpected interactions
+ handling large numbers of requests

#### mitigation, use well known
+ primitives and patterns
+ libraries
+ (java) connection pools
+ (java) object registries

#### mitigation
+ try to keep request-handling threads from blocking each other
+ keep domain object immutable
+ Command Query Responsibility Separation

### synchronizing

+ Liskov substitution principle (see Family Values: A Behavioral Notion of Subtyping [LW93]) states that any property that is true about objects of a type T should also be true for objects of any subtype of T. In other words, a method without side effects in a base class should also be free of side effects in derived classes.
+ Synchronization: A Java keyword that makes Only one thread may execute inside the method at a time. It can fail because one single thread was inside the create call, waiting for a response that would never come.

## Self Denial

Anna Kournikova Trojan
Morris worm

hundreds of servers all serialized waiting for a      write lock on one item.

### Avoiding Self-Denial

## Scaling effects

square cube law

### Point to point communication
+ n^2 connection scaling
+ pay for test environment same size as production environment cost prohibitive

#### alternatives to point-to-point
+ UDP broadcast - everyone gets it
+ TCP/UDP multicast - only interested parties
+ pub/sub - best but expensive

### Shared Resources
+ shared resource bottleneck
+ shared nothing

#### shared nothing failover
+ coordination between failing and new server
+ session backup server
+ database table
+ shared sessions

## unbalanced capacities
+ throttled apis
+ front end overwelming the back end
+ circuit breaker
+ backpressure/handshaking to get callers to throttle
+ bulkheads

### drive out through testing
+ test harneses
+ capacity modeling
+ test tough workloads - fail fast
+ autoscaling

## dogpile
+ booting several servers at once
+ cron job
+ configuration management ? pushes new change
+ blocked threads become unblocked

## force multiplier
### outage amplification
+ package management system restarts autoscaler
+ service discovery can't access systems and propigates to other service discovery
+ control plane forms beliefs about the system state and takes action

## controls and safeguards
+ Likely an observer problem if 80% of the system is unavailable
+ Shut down old systems slowly
+ Get confermatoin if expected state diverges from observed state
+ Lag time and momentum

## slow responses
+ memory leaks
+ network congestion WAN
+ TCP stalls low level socket protocols don't look till recieve buffer is empty
+ monitor system performance
+ document the conditions in which you can refuse requests

## unbounded result sets
+ database/back end can unexpetedly overwelm your query with millions of responses
+ limit the size or number of results you are willing to accept
+ audit trails are especially vulnerable
+ power law vs normal distribution
+ break out after reaching maximum - wastes resorces but works

# Chapter 5

## Timeouts
+ use a generic gateway to provide the template for connection handling, error handling, query execution, and result processing
+ make full use of your platform, e.g. Amazon API Gateway
+ fast retries usually fail
+ queuing the work for a slow retry later
+ circuit breaker if too many retries
+ fail fast is for incoming requests
+ timeout primarily for outbound requests

## Circuit Breaker
+ closed -> retry -> open
+ tracks failures
+ involve stakeholders
+ counting faults that are closely clustered in time
+ in multiprocessing, each process has own CB
+ use a CB library
+ CBs must be reported somehow

### Open state behaviors
+ imediately fail
+ return last known good value
+ generic answer
+ secondary service

## Bulkheads
+ most common: redundancy
+ have separate server groups for mission critical service calls and non-mission critical
+ partition clusters of the same application

## Steady State
+ immutable infrastructure in prod
+ data purging
+ log file rotation
+ Unix logrotate
+ complience: get logs off of production machines as quickly as possible and store them on a centralized server
+ logstash
+ is the space of possible keys finite or infinite?
+ Do the cached items ever change?
+ Purge data with application logic?

## Fail Fast
+ don't queue failed requests
+ service should check all integration points and circuit breakers before processing a request
+ parameter checking
+ system vs application failures

## Let it Crash
+ take system back to startup state
+ exception handlers

### Cleaning up Memory
+ try-finally blocks
+ block-scoped resources

### Limited Granularity
+ resource management
+ state isolation
+ actors in erlang, elyxir, akka, java, scala

### Fast Replacement
+ if startup time is measured in miliseconds

### Supervision
+ supervisor trees

### Reintegration
+ resume calling the newly restored provider

# Handshaking

## HTTP Protocols
+ XML-RPC
+ WS-I Basic

## Remote Procedure Call Protocols
+ CORBA
+ DCOM
+ Java RMI

+ healthchecks to determine if a reciever can recieve requests
+ request caller to back off
+ best to implement custom handshaking protocols
+ circuit breaker, track call to see if it worked

# Test Harnesses
+ emulates the remote system on the other end of each integration point
+ runs as a separate server
+ executes outside of spec, invoking errors

# Decoupling Middleware
+ synchronous vs asynchronous design
+ middleware tends to by synchronous because it's easier

# Shed Load
+ TCP connect queue can fill up and will refuse connection
+ applications can do the same thing, fail fast
+ performance relative to SLA
+ semaphore to only accept N connections
+ send 503 service unavailable to load balancer

# Back Pressure
+ telling upstream to slow down
+ only use inside system boundary
  + accross boundary, use shedding
+ use finite queues

# Governors
+ look at automation and make sure the system is running within it's boundaries


## Previous

+ Be aware of operational and development costs
  + When our service go down we can potentially lose audience member
  + 10% of users can help us make our services better
  + Regulatory expenses are per errors
  + Incremental improvement as apposed to just "fixing the problem"
  + Put dollar amounts on what we do
  + Developers should be able to communicate dollar amounts for operational costs and development cost
+ Could be helpful to provide scripts to NOC to dump logs etc, before a developer is called.
+ Do code reviews with risks and external dependancies
+ More robust error handling with more 400 error codes
    + For example explicitly call out authentication errors
+ Start looking at circuit breaker
+ Determine our performance requirements
+ Determine when we will start autoscaling
+ Continuous Testing
  + We have cps automated tests that run every hour
  + Impulse testing
+ QA suite should run on the QA box
+ Blue Green developer
  + Deploy bleeding edge code to a smaller group group "A"
  + If that goes well we would expand this group to more people "B"
  + Must have a robust Fail Back strategy


Fortunately, the team had created scripts long ago to take thread      dumps of all the Java applications and snapshots of the databas

ither did URL probing.

“you touched it last”

A postmortem is like a murder mystery

The backup ran before the outage, so that would tell me whether any      configurations were changed between the backup and my investigation. In      other words, that would tell me whether someone was trying to cover up a      mistake.

I took the binaries from production and decompiled them

 The key lesson to be drawn      here, though, is that the JDBC specification allows      java.sql.Statement.close to throw a      SQLException, so your code has to handle it.

 hey      cannot—must not—allow bugs to cause a chain of failures. We’re going      to look at design patterns that can prevent this type of problem from      spreading.

 Enterprise software must be cynical.

 impulse

 just keep driving requests all the time. (Also, be sure to have the scripts slack for a few hours a day to simulate the slow period during the middle of the night. That will catch connection pool and firewall timeouts.

The original trigger      and the way the crack spreads to the rest of the system, together with the      result of the damage, are collectively called a failure      mode.

timeouts

Conversely, the less-coupled      architectures act as shock absorbers, diminishing the effects of this error      instead of amplifying them.

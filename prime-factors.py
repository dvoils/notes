def primeFactors(n):
    p = []
    while n % 2 == 0:
        p.append(2)
        n = n / 2
          
    # i = i + 2
    for i in range(3,int(math.sqrt(n))+1,2):
        while n % i== 0:
            p.append(i)
            n = n / i
              
    # if n is a prime > 2
    if n > 2:
        p.append(n)
    
    return p

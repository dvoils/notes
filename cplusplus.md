## Makefile info

+ A great [tutorial](http://mrbook.org/blog/tutorials/make/)
+ What stuff [means](https://stackoverflow.com/questions/3220277/what-do-the-makefile-symbols-and-mean)
  + `all:` library.cpp main.cpp
  + `$@` evaluates to all
  + `$<` evaluates to library.cpp
  + `$^` evaluates to library.cpp main.cpp
+ GNU's take on all [this](https://www.gnu.org/software/make/manual/make.html)
+ Make is very picky about tabs


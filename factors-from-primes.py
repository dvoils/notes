import math
import itertools

def mult(x,y):
    return x * y

def primeFactors(n):
    p = []
    while n % 2 == 0:
        p.append(2)
        n = n / 2
          
    # i = i + 2
    for i in range(3,int(math.sqrt(n))+1,2):
        while n % i== 0:
            p.append(i)
            n = n / i
              
    # if n is a prime > 2
    if n > 2:
        p.append(n)
    
    return p

def factorsFromPrimes(n, p):
    l = len(p)
    r = []
    for g in range(1,n):
        c = list(itertools.combinations(p,g))
        for q in c:
            m = reduce(mult, q)
            if m not in r:
                r.append(m)
    return r

n = 84632
p = primeFactors(n)
print factorsFromPrimes(n, p)
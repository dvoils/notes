# Misc dev
+ [reasonml](https://reasonml.github.io/)
+ [setup-zfs-storage-pool](https://ubuntu.com/tutorials/setup-zfs-storage-pool#1-overview)
+ [git annex](https://git-annex.branchable.com/)
+ [rclone](https://rclone.org/)
+ [backblaze](https://www.backblaze.com/)

# Rust
[wasm-vue](https://github.com/greenpdx/wasm-vue)

# Language Processing
+ [Stanford NLP with Deep Learning](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1184/syllabus.html)
+ [Global Vectors for Word Representation](https://nlp.stanford.edu/projects/glove/)

# IOT
+ [gpsd](https://gpsd.gitlab.io/gpsd/index.html)
+ [telemetrix](https://github.com/protometa/telemetrix)
+ [how-to-ssh-into-your-raspberry-pi-with-a-mac-and-ethernet-cable](https://medium.com/@tzhenghao/how-to-ssh-into-your-raspberry-pi-with-a-mac-and-ethernet-cable-636a197d055)
+ [IPv6](https://en.wikipedia.org/wiki/IPv6)
+ [gpsd](https://gpsd.gitlab.io/gpsd/index.html)

# Node
+ [generators](https://strongloop.com/strongblog/how-to-generators-node-js-yield-use-cases/)


## Blogs
[Matt Baker's math blog](https://mattbaker.blog/)

## e commerce
### udemy class
+ [udemy react-node-ecommerce](https://www.udemy.com/react-node-ecommerce)
+ [source](https://github.com/kaloraat/react-node-ecommerce)
## Programming
+ [JavaAid Youtube](https://www.youtube.com/channel/UCx1hbK753l3WhwXP5r93eYA/playlists?disable_polymer=1)
+ [Python Linear Algebra](https://docs.scipy.org/doc/numpy-1.15.0/reference/routines.linalg.html#matrix-and-vector-products)
+ [functional javascript part1: lists](https://blog.jeremyfairbank.com/javascript/functional-javascript-lists-1/)
+ [Make Tutorial](http://mrbook.org/blog/tutorials/make)

## DevOps
+ [terraform Infrastructure-As-Code-Intro](https://github.com/Zelgius/Infrastructure-As-Code-Intro)
+ [pulumi](https://www.pulumi.com/)
+ [setting up continuous integration continuous deployment with jenkins](https://code.tutsplus.com/tutorials/setting-up-continuous-integration-continuous-deployment-with-jenkins--cms-21511)
+ [zeit now](https://zeit.co/now)
+ [okta](https://developer.okta.com/product/)
+ [ory](https://www.ory.sh/run-oauth2-server-open-source-api-security)

## Spark
+ [scala-spark-tutorial-1-hello-world](https://medium.com/luckspark/scala-spark-tutorial-1-hello-world-7e66747faec)

## Electric Cars
+ [teknomadix](https://teknomadix.com/)
+ [argos](http://www.agros2d.org)
+ [FEniCS](https://fenicsproject.org/)
+ [SfePy](sfepy.or)

## Model Building
+ [which gpu for deep learning](https://timdettmers.com/2019/04/03/which-gpu-for-deep-learning/)
+ [paraview](https://www.paraview.org/)
+ [brian spiking models](https://senselab.med.yale.edu/modeldb/ModelList.cshtml?id=113733&describe=yes&celldescr=&&allsimu=)

## Coding Interview Resources
+ [udemy coding-interview-bootcamp-algorithms-and-data-structure](https://www.udemy.com/coding-interview-bootcamp-algorithms-and-data-structure/learn/lecture/8546970?start=15#overview)
+ [javascript examples](https://github.com/StephenGrider/AlgoCasts)
+ [algoexpert](https://www.algoexpert.io/product)
+ [daily coding problem](https://dailycodingproblem.com/)

## Dependency Injection
+ [dependency injection for beginners](https://blog.usejournal.com/dependency-injection-for-beginners-56c643363e92)

## Kubernetes
+ [creating image pull secret for gcr](https://stackoverflow.com/questions/36283660/creating-image-pull-secret-for-google-container-registry-that-doesnt-expire)
+ [kubectl run with imagepullsecrits](https://ekartco.com/2017/10/kubernetes-kubectl-run-with-imagepullsecrets/)
+ [dockerizing a react application](https://medium.com/ai2-blog/dockerizing-a-react-application-3563688a2378)
+ [images](https://kubernetes.io/docs/concepts/containers/images/)
+ [using google container registry](https://ryaneschinger.com/blog/using-google-container-registry-gcr-with-minikube/)
+ [katacoda](https://www.katacoda.com/)
+ [kubernetes external ip](https://stackoverflow.com/questions/44110876/kubernetes-service-external-ip-pending)
+ [kubernetes raspberri pi](https://github.com/kubernetes/kube-deploy/issues/220)
+ [kubernetes bare metal](https://itnext.io/install-kubernetes-on-bare-metal-centos7-fba40e9bb3de)
+ [access pods outside of cluster](http://alesnosek.com/blog/2017/02/14/accessing-kubernetes-pods-from-outside-of-the-cluster/)
+ [connect application to services](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/#creating-a-service)
+ [gcloud container registry quickstart](https://cloud.google.com/container-registry/docs/quickstart)
+ [gcloud ubuntu](https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu)
+ [install gcloud SDK](https://cloud.google.com/sdk/docs/downloads-apt-get)
+ [private container registry](https://github.com/kubernetes/kubernetes/tree/release-1.9/cluster/addons/registry)
+ [create docker image](https://www.mirantis.com/blog/how-do-i-create-a-new-docker-image-for-my-application/)
+ [kubectl commands](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-)
+ [kubectl deploy app](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)

## Mongo
+ [docker container with auth](https://medium.com/@anuradhs/connect-to-mongodb-docker-container-with-authentication-using-mongoose-and-nodejs-6319bea82e9d)
+ [authentication](https://medium.com/@anuradhs/how-to-start-a-mongo-database-with-authentication-using-docker-container-8ce63da47a71)
+ [more on authentication](https://medium.com/mongoaudit/how-to-enable-authentication-on-mongodb-b9e8a924efac)
+ [setting up an admin](https://medium.com/@yasiru.13/mongodb-setting-up-an-admin-and-login-as-admin-856ea6856faf)
+ [stackoverflow mongo startup](https://stackoverflow.com/questions/42912755/how-to-create-a-db-for-mongodb-container-on-start-up)
+ [mongo docker container](https://hub.docker.com/_/mongo)
+ [secure mongodb](http://people.oregonstate.edu/~chriconn/sites/docker_mongoDB/)

## React
+ [dropdown example](https://github.com/dbilgili/Custom-ReactJS-Dropdown-Components)
+ [react tutorial](https://reactjs.org/docs/hello-world.html)
+ [react-webpack-simple](https://github.com/jsphbtst/react-webpack-simple)

## AWS
+ [how-to-create-an-aws-iam-policy-to-grant-aws-lambda-access-to-an-amazon-dynamodb-table](https://aws.amazon.com/blogs/security/how-to-create-an-aws-iam-policy-to-grant-aws-lambda-access-to-an-amazon-dynamodb-table/)
+ [api-gateway-create-api-as-simple-proxy-for-lambda](https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html)
+ [dynamodb createTable](https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_CreateTable.html)
+ [stackoverflow dynamodb-how-to-query-a-global-secondary-index](https://stackoverflow.com/questions/51134296/dynamodb-how-to-query-a-global-secondary-index)
+ [building-fine-grained-authorization-using-amazon-cognito-user-pools-groups](https://aws.amazon.com/blogs/mobile/building-fine-grained-authorization-using-amazon-cognito-user-pools-groups/)
+ [build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/](https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/)

## Web Application
+ [serverless-stack](https://serverless-stack.com/)
+ [webpack-tutorials](https://www.valentinog.com/blog/webpack-tutorial/)
+ [shopping cart](https://github.com/mrvautin/expressCart)
+ [simple react todo](https://github.com/aghh1504/simple-react-todo-list#npm-start)
+ [express-js-produces-404-for-js-file-that-clearly-exists](https://stackoverflow.com/questions/27565411/express-js-produces-404-for-js-file-that-clearly-exists)
+ [whats-the-correct-way-to-serve-production-react-bundle-js-built-by-webpack](https://stackoverflow.com/questions/35928766/whats-the-correct-way-to-serve-production-react-bundle-js-built-by-webpack)
+ [simple_webpack_boilerplate](https://github.com/pinglinh/simple_webpack_boilerplate)
+ [part-1-react-app-from-scratch-using-webpack-4](https://medium.freecodecamp.org/part-1-react-app-from-scratch-using-webpack-4-562b1d231e75)
+ [node express mongo](https://github.com/DanWahlin/NodeExpressMongoDBDockerApp)
+ [react docker](https://www.telerik.com/blogs/dockerizing-react-applications-for-continuous-integration)
+ [scotch post parameters](https://scotch.io/tutorials/use-expressjs-to-get-url-and-post-parameters)
+ [mongo node crud](https://codeburst.io/writing-a-crud-app-with-node-js-and-mongodb-e0827cbbdafb)
+ [Hapi Mongo](https://patrick-meier.io/build-a-restful-api-using-hapi-js-and-mongodb/)
+ [Mongo and Docker](https://hub.docker.com/_/mongo/)
+ [yanb](https://github.com/GenFirst/yanb)
+ [Docker Node Mongo](https://github.com/KrunalLathiya/DockerNodeMongo)
+ [MEAN example](https://github.com/GenFirst/yanb)
+ [forms](https://stackoverflow.com/questions/133925/javascript-post-request-like-a-form-submit)
+ [CRUD Express MongoDB](https://zellwk.com/blog/crud-express-mongodb/)
+ [Express, Node and Docker](https://github.com/mpayetta/express-node-docker)

## Misc
+ [Josan Gonzalez](https://www.artstation.com/josan)
+ [Josan Gonzalez Deviant Art](https://www.deviantart.com/f1x-2)

## Parts
+ [Boundary Devices](https://boundarydevices.com/)
+ [kendryte](https://kendryte.com/)
+ [superbrightleds](https://www.superbrightleds.com)
+ [Mobile Defenders](https://www.mobiledefenders.com)
+ [Cellular Parts USA](http://www.cellularpartsusa.com)
+ [Electro-Gems](https://www.electrogems.com/)
+ [Google Developer Web Fundamentals](https://developers.google.com/web/fundamentals/)
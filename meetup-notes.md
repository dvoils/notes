# Brainsnail
+ logan davis
+ 3 stories of failure 
+ therac 25
  + xrays
  + electron beam

## circuit breaker
+ health check caches 

## developers training
+ ada develpers acadamy
+ epicous 
+ alchemy code lab
+ pdx code guild
+ psu pcep 
+ mecop





# QA
+ communication colaberation support

## Chalenges
+ change management
+ slow processes
+ listen to feeback
+ include "why"
    + identify pain points
+ Laura Hogan 

## Onboarding
+ make it easy
+ use a makefile that encompases all scripts
+ I do, we do, you do 
+ documentation 
 + test specific read-me 
 + testing templates 

# Architects 
+ Design for test 

# Product managers
+ quality is worth time and money 

# Developers
+ TDD 
+ Code reviews, reduce siloing
+ vaidehi joshi

# Testers
+ dedicated testers 


# Misc links
+ [kubecon](https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2018/)
+ [signal sciences](https://www.signalsciences.com/)

# Layers 
+ 7 layers 
+ thrift, redis, grpc
+ front-proxy service mesh

## Turbine
+ [turbine](https://www.turbinelabs.io/)

## envoy
+ slowly decompose monolith

## istio
+ [istio](https://istio.io/)
+ pilot, mixer, citidel

## Dynamic config
+ blue green deployment
+ based on headers and cookies
+ percent to different entities 
+ backbpressure and circuit breaking 

## Observibility
+ Service level objective

# Monolithic -vs- Microservice Architecture

+ soa and esb 
+ api management 
    + openapi, swagger

## esb and application layer are monolithic 
+ services 
+ portal 1, portal 2 
+ api management 
+ services 

## esb - virtual service layers 
+ circuit breakers 

## multiple requirements for a single application 
+ checkout servises 

## scale independantly 
+ segregate businiss functions into services!!!
deploy and scale
team autonomy
how to integrate?

## microservice 
+ business logic and interprocess communication 
+ multiple languages 
+ talks to multiple backends 

## orchistration 
+ 1 api requires 10 differenct back end calls 
+ single api gateway 
+ multiple apis 
+ netflix-techblog 

## active composition/orchestration the following is a hierarchical tree
+ consumers, api services, composit services, core services

## api services
+ security caching etc,
+ request response
+ grpc 


## sychronouse reactive compostiontion choreoraphy patterern 

## application, to event bus
+ talks to services via a queue like rebbit mc 

##pub/sub
+ single producer
+ multiple consumer 
+ publish to a topic to subscribers 
+ kafka, brokers
+ no business logic in broker 

## event sorucing 
+ publis and subscribe events 
+ state of application goes to central event bus 
+ multiple consumers
+ recreate event status 
+ multiple subscrivers 
+ consumers can recreate??
+ kafka

## cqrs 
+ crud model is not allways feasable
+ split the ciumunication data model
+ query model, command model 

## active vs reactive
+ what is the best pattern 
+ synchronous event driven etc ?
+ event driven

## anti corruption layer pattern 
+ isolate the lagacy and modern systems using an anti-corruption layer 

## strangler pattern 
+ incrementally replace 
+ create facade layer 
+ integrate microservice with a central esb 
+ integrate microservices 

# ballerena
## cloud native programming language 
+ esb, bpmn, eai not agile 
+ java, node not integration simple 
+ code over config 
+ sequence diagrams ??
+ graphical, and code
+ don't hide the network 
+ json, xml, types
+ secure by defauld, no sql injection??
+ network circuit breaking default 
+ apache licence 2


## question
+ how is this done? how to install? how to scale? how to build the architecture?
+ what language does it compile to?
+ ignore error??
+ how to debug?
+ why grpc?
+ linker c?
+ queue?
+ linking other languages
+ functional, classes, datastructures?

# Metaparticle
+ code and configuration is how it's done now
+ deployment steps 
+ standard library on top of kubernetes 
+ kubectl get pods
+ kubectl get svc

# Istio Service Mesh 
+ network resiliancy 
+ service mesh non buisiness code
+ control plane - yaml file 
+ balerina can offload stuff to service mesh 

+ codefresh 


+ samsung 1icp6/57/61 battery
+ bus leaves hillsborro at 5:45

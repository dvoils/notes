## Notes
+ [Super Awesome Git Cheetsheet](git.md)
+ [Docker and Kubernetes](https://gitlab.com/dvoils/docker-examples/blob/master/README.md)
+ [Algorithms and Math](https://gitlab.com/dvoils/algorithms/blob/master/home.md)
+ [Coding Interview Solutions](https://gitlab.com/dvoils/interview-questions)
+ [Spark Setup](https://gitlab.com/dvoils/spark-examples/blob/master/setup.md)
+ [Links](links.md)
+ [Misc](notes.md)


# Super Awesome GIT Cheatsheet

## branching
+ `git branch`
+ `git branch <branch>`
+ `git checkout <branch>`
+ `git checkout -b <branch>`
+ `git add <changes>`
+ `git commit -m "description"`

## track a branch
+ `git push origin <branch>`
+ `git push --set-upstream origin <branch>`

## delete local changes
+ git reset --hard origin/master or <branch>

## create new repo based on an old one
+ git remote set-url origin `repo url`
+ git remote -v

## delete local changes
+ git reset --hard origin/master or `some branch`

## get remote changes
+ git pull origin <branch>
+ or just `git pull`

## delete a branch
### delete branch locally
+ git branch -d localBranchName

### delete branch remotely
+ git push origin --delete remoteBranchName

## pull changes from master to branch
+ git rebase master
+ git pull
+ git push

## new GitLab project from existing directory
+ cd into directory
+ `git init`
+ `git add` required files
+ `git commit`
+ `git remote add origin git@gitlab.com:<username>/<repo>.git`
+ `git push -u origin master`

## revert to a previous branch
+ git reset --hard <commit-hash>
+ git push -f origin master

## Merge development branch with master
+ `git fetch`
+ `git branch --track <branch> origin/<branch>`
+ on branch `git merge master`
+ resolve any merge conflicts if there are any
+ `git checkout master`
+ `git merge <branch>` there won't be any conflicts

